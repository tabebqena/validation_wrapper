from enum import Enum
from functools import wraps
from typing import Any, Callable, Union


class ValidationResult:
    data: object = None
    valid: bool = False
    result: object = None
    exception: Union[BaseException, None] = None


class ResultNotAsExpectedException(BaseException):
    def __init__(self, result, *args: object) -> None:
        super().__init__(result, *args)
        self.result = result


class ValidationError(BaseException):
    def __init__(self, validation_result, *args: object) -> None:
        super().__init__(validation_result, *args)
        self.validation_result = validation_result


class InvalidStrategy(Enum):
    STOP = 1
    RAISE = 2
    CALL = 3


class InvalidCallbackNotEpecified(NotImplementedError):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ValidationWrapper:
    def __init__(
        self,
        data: Any = None,
        data_key: str = "data",
        schema: object = None,
        schema_validate_method: str = "validate",
        schema_args=(),
        schema_kwargs={},
        valid_value: object = True,
        on_invalid: Callable = None,
        on_invalid_args=(),
        on_invalid_kwargs={},
        invalid_strategy: InvalidStrategy = InvalidStrategy.RAISE,
        validation_result_arg: str = None,
        **kwargs,
    ) -> None:
        """
        __init__ Wrapper around any validation schema,

        [TODO]

        :param data: The data to validate, You can omit this when use in decorators,
            In that case, the `data_id` argument will be used, defaults to None
        :type data: Any, optional
        :param data_key: when you use this class as decorator, you can use `data_key` argument,
         to specify which argument of the called decorated method will contain the data to validate.
         if you specify this argument as `{}` all arguments passed to the decorated methos will be used as the
         data.
          defaults to "data"
        :type data_key: str, optional
        :param schema: the schema used for validation of `data`,
           this may be any object that has method named `validate`
           or the name specified by `schema_validate_method`, defaults to None
        :type schema: object, required. If omitted or equals `None` Exception will raised.
          N.B:. schema should be instance object not class.
        :param schema_validate_method: The method that will be
        called to validate the data,
        by default this equals 'validate' so the method `schema.validate` will be called,
        You can override this and specifiy specific method name.
        if the schema supplied not containing method named 'validate' nor the name specified
        by the `schema_validate_method`, exception will raised
        default=`validate`
        :type schema_validate_method: str, optional
        :param schema_args: the arguments should be passed to the `schema.validate` method,
         when you supply this argument, the `schema.validate(*schema_args)` will be called.
         This may be useful when the `schema.validate` needs extra arguments. ex: `session`
         defaults to ()
        :type schema_args: tuple, optional
        :param schema_kwargs: the keword arguments that should be passed to the
        `schema.validate` method,
         when you supply this argument, the `schema.validate(*schema_args, **schema_kwargs)` will be called.
         This may be useful when the `schema.validate` needs extra keyword arguments. ex: `session`
        , defaults to {}
        :type schema_kwargs: dict, optional
        :param valid_value: The value that `schema.validate` method will return if the data is valid
        for example: some schemas return dictionary of invalid values. others return `True` or `False`
         defaults to True
        :type valid_value: object, optional
        :param on_invalid: method that will be called if the data is invalid,
        this method will recieve the `validation_result` object as the first argument. the `*on_invalid_args`
        as the positiona arguments and the `on_invalid_kwargs` as the keyword arguments
        defaults to None
        :type on_invalid: Callable, optional
        :param on_invalid_args: positional arguments that will passed to `on_invalid` method , defaults to ()
        :type on_invalid_args: tuple, optional
        :param on_invalid_kwargs: keyword arguments that will passed to `on_invalid` method , defaults to {}
        :type on_invalid_kwargs: dict, optional
        N.B:. the on_invalid will be called as :`on_invalid(*on_invalid_args, **on_invalid_kwargs)`
        :param invalid_strategy: What to do after calling the `on_invalid` callback.
           - InvalidStrategy.RAISE means raise `ValidationError` exception,
           - InvalidStrategy.CALL means call the decorated method
           - InvalidStrategy.STOP means do nothing (don't call the method nor raise exception).
           N.B:. the `STOP` strategy will raise `InvalidCallbackNotEpecified` exception if you didn't specify `on_invalid` method.
        defaults to InvalidStrategy.RAISE
        :type invalid_strategy: InvalidStrategy, optional
        :param validation_result_arg: when you specify this arguments the validation result will be passed to
        the decorated method, as value for the validation_result_arg argument, defaults to None
        N.B:. don't forget to let the decorated method accept keyword argument you specified by the
        `validation_result_arg`.
        :type validation_result_arg: str, optional
        :param kwargs: additiona keyword arguments.
        :type kwargs: dict, optional

        :raises Exception: "Schema couldn't be None,"
        :raises Exception: "The valiadator couldn't be None". the validator is the `schema.validate`
        or `schema` method that has the name you specified by `schema_validate_method`
        :raises Exception: "The invalid strategy should be one of the InvalidStrategy enum"

        """
        self._data = data
        self._data_key = data_key or {}
        self._schema = schema
        self._schema_args = schema_args or ()
        self._schema_kwargs = schema_kwargs or {}
        self._valid_result = (
            lambda: valid_value if valid_value != None else {}
        )()
        self._on_invalid_method = on_invalid
        self._on_invalid_args = on_invalid_args
        self._on_invalid_kwargs = on_invalid_kwargs
        self.__invalid_strategy = invalid_strategy
        self.__validation_result_arg = validation_result_arg

        self._kwargs = kwargs or {}

        if not schema:
            raise Exception(
                "Schema couldn't be None, valid values is object that has `validate` method, \
                    or has method with the name you specified in the `validator` argument"
            )
        if schema_validate_method:
            self._validator = getattr(schema, schema_validate_method, None)
        if not self._validator:
            self._validator = getattr(schema, "validate", None)
        if not self._validator:
            raise Exception(
                "The valiadator couldn't be None\
                valid value is any callable either the method `validate` of schema object\
                or the schema method which its name is equal to `schema_validate_method` argument  \
                "
            )
        if self.__invalid_strategy.value not in list(
            item.value for item in InvalidStrategy
        ):
            raise Exception(
                "The invalid strategy should be one of: InvalidStrategy.RAISE, InvalidStrategy.CALL or \
                    InvalidStrategy.STOP"
            )

    def get_data(self, **kwargs):
        data = None
        if self._data:
            data = self._data
        if self._data_key == {}:
            data = kwargs
        elif self._data_key:
            data = kwargs.get(self._data_key, {})
        else:
            raise Exception("You should specify `data` or `data_key` arguments")

        return data

    def __call_validator__(self, data, valid_result) -> ValidationResult:
        valid_result = valid_result or self._valid_result
        validation_result = ValidationResult()
        validation_result.data = data
        try:
            result = self._validator(
                data, *self._schema_args, **self._schema_kwargs
            )
            validation_result.result = result
            if result == valid_result:
                validation_result.valid = True
                return validation_result
            raise ResultNotAsExpectedException(result)
        except ResultNotAsExpectedException as e:
            validation_result.result = e.result
            validation_result.exception = None

        except BaseException as e:
            validation_result.result = None
            validation_result.exception = e

        validation_result.valid = False
        return validation_result

    def on_invalid(self, validation_result: ValidationResult):
        if self._on_invalid_method:
            return self._on_invalid_method(
                validation_result,
                *self._on_invalid_args,
                **self._on_invalid_kwargs,
            )

        raise InvalidCallbackNotEpecified

    def __call__(self, func, *args, **kwargs):
        @wraps(func)
        def internal(*method_args, **method_kwargs):
            data = self.get_data(**method_kwargs)
            validation_result: ValidationResult = self.__call_validator__(
                data, self._valid_result
            )

            if validation_result.valid:
                if self._data:
                    pass
                    # return func(*method_args, **method_kwargs)
                elif self._data_key == {}:
                    pass
                elif self._data_key != {}:
                    method_kwargs[str(self._data_key)] = data
                    # return func(*method_args, **method_kwargs)
                if self.__validation_result_arg:
                    method_kwargs[
                        self.__validation_result_arg
                    ] = validation_result
                return func(*method_args, **method_kwargs)
            else:
                invalid_callback_specified = True
                try:
                    self.on_invalid(
                        validation_result,
                        *self._on_invalid_args,
                        **self._on_invalid_kwargs,
                    )
                except InvalidCallbackNotEpecified:
                    invalid_callback_specified = False

                if self.__validation_result_arg:
                    method_kwargs[
                        self.__validation_result_arg
                    ] = validation_result

                if self.__invalid_strategy == InvalidStrategy.RAISE:
                    raise ValidationError(validation_result)
                elif self.__invalid_strategy == InvalidStrategy.STOP:
                    if not invalid_callback_specified:
                        raise InvalidCallbackNotEpecified(
                            "Invalid callback not specified &"
                            "InvalidStrategy == STOP, This exception raised to capture"
                            "this situation where nor invalid callback nor your"
                            "decorated method will called."
                            " to mute this error override the on_invalid method"
                            "or change the onvalid_strategy value"
                        )
                elif self.__invalid_strategy == InvalidStrategy.CALL:
                    return func(*method_args, **method_kwargs)

        return internal
