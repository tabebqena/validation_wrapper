#!/usr/bin/env python3

from distutils.core import setup

setup(
    name="validation_wrapper",
    version="0.2.1",
    description="Python decorator for data validation."
    " used as wrapper around 'schema'",
    author="Ahmad Yahia",
    author_email="tabebqena@gmail.com",
    # packages=["."],
    packages=["validation_wrapper"],
    package_dir={"validation_wrapper": "validation_wrapper"},
)
