from validation_wrapper import (
    ValidationWrapper,
    ValidationResult,
    ValidationError,
    InvalidStrategy,
)
import unittest
import pytest
from unittest.mock import Mock


class SchemaMock(object):
    def validate(self, data, **validation_kwargs):
        return True


class TestValidationWrapper(unittest.TestCase):
    def setUp(self) -> None:
        self.schema = SchemaMock()
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def testvalid_data_key(self):
        expected = {1: "1", 2: "2"}

        @ValidationWrapper(
            data_key="data",
            schema=self.schema,
            validation_kwargs={},
            valid_value=True,
        )
        def decorated(**kwargs):
            print(kwargs)
            return kwargs.get("data", {}) == expected

        self.assertEqual(decorated(data=expected), True)

    def testvalid_data(self):
        expected = {1: "1", 2: "2"}

        @ValidationWrapper(
            data=expected,
            schema=self.schema,
            validation_kwargs={},
            valid_value=True,
        )
        def decorated(**kwargs):
            print(kwargs)
            return kwargs.get("data", {}) == expected

        self.assertEqual(decorated(data=expected), True)

    def test_invalid_strategy(self):
        expected = {1: "1", 2: "2"}

        class Fake:
            value = 5

        with self.assertRaises(Exception):

            @ValidationWrapper(
                data=expected,
                schema=self.schema,
                validation_kwargs={},
                valid_value=True,
                invalid_strategy=Fake(),
            )
            def decorated(**kwargs):
                print(kwargs)
                return kwargs.get("data", {}) == expected

    def test_valid_bracket_data_key(self):
        expected = {"one": "1", "two": "2"}

        @ValidationWrapper(
            data_key={},
            schema=self.schema,
            validation_kwargs={},
            valid_value=True,
        )
        def decorated(**kwargs):
            return kwargs == expected

        self.assertEqual(decorated(**expected), True)

    def test_none_data_key(self):
        with self.assertRaises(Exception) as cm:

            @ValidationWrapper(
                data_key=None,
                schema=self.schema,
                validation_kwargs={},
                valid_value=True,
            )
            def decorated(**kwargs):
                return True

            assert decorated() != True

    def test_none_schema(self):
        with self.assertRaises(Exception) as cm:
            ValidationWrapper(
                data_key="data",
                schema=None,
                validation_kwargs={},
                valid_value=True,
            )

    def test_with_validator_keyword(self):
        class S:
            def custom_validator(self, data, **validation_kwargs):
                return True

        schema = S()

        wrapper = ValidationWrapper(
            data_key="data",
            schema=schema,
            validation_kwargs={},
            valid_value=True,
            schema_validate_method="custom_validator",
        )
        assert wrapper._validator == schema.custom_validator

    def test_with_no_validator_no_validate_method_arg(self):
        class S:
            def custom_validator(self, data, **validation_kwargs):
                return True

        schema = S()
        with self.assertRaises(Exception) as cm:
            ValidationWrapper(
                data_key="data",
                schema=schema,
                validation_kwargs={},
                valid_value=True,
                schema_validate_method=None,
            )

    def test_with_no_validator(self):
        class S:
            def custom_validator(self, data, **validation_kwargs):
                return True

        schema = S()
        with self.assertRaises(Exception) as cm:
            ValidationWrapper(
                data_key="data",
                schema=schema,
                validation_kwargs={},
                valid_value=True,
            )

    @pytest.mark.slow
    def test_unequal_value(self):
        @ValidationWrapper(
            data_key="data",
            schema=self.schema,
            validation_kwargs={},
            valid_value=[],
            invalid_strategy=InvalidStrategy.CALL,
        )
        def decorated(**kwargs):
            return "expected_result"

        self.assertEqual(decorated(), "expected_result")

    def test_on_error_called(self):
        f = Mock()

        class S:
            def validate(self, data, **validation_kwargs):
                raise Exception()

        schema = S()

        class CustomValidationWrapper(ValidationWrapper):
            def on_invalid(self, result):
                f()

        @CustomValidationWrapper(
            data_key="data",
            schema=schema,
            validation_kwargs={},
            invalid_strategy=InvalidStrategy.CALL,
        )
        def decorated(*args, **kwargs):
            print(args, kwargs)

        decorated()
        f.assert_called()
